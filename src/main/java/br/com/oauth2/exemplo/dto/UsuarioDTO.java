package br.com.oauth2.exemplo.dto;

public class UsuarioDTO {

	private String nome;
	private String userName;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
